<!-- $theme: gaia -->

# ![150%](img/noser-coin.svg)

---

# Noser Crypto-Hedgefonds
- 23 Investoren 
- Einlagen @  CHF 100.-
- Fonds ist geschlossen bis 30.09.2018

---
# And then this happened:

![140%](img/total_market_cap.png)

---

# Performance

![bg](img/noser-coin_transp.svg)
![80%](img/web.png)

http://cryptofund.azurewebsites.net

Total Portfolio Value is **CHF 9177.-**

(@~19.12.2017)

---

# Performance #2
![bg](img/noser-coin_transp.svg)
![120%](img/portfolio_evolution.png)

 
---

# Bubble?

![165%](img/bubble.jpg)

---

# Fabian's Opinion
- Das ist erst der Anfang
- "Cryptos are here to stay"
- Mass Adaption hat noch nicht stattgefunden
- Aber Vorsicht: Do your own research!
  - Vieles ist aktuell wohl überbewertet
  - Betrug, Scam, Fishing etc
- Die Frage it: was wird sich durchsetzen?

---

# All for nothing
Still hosted in the smoggy Cloud:
https://gitlab.com/unSinn/presentation-crypto-fondsTwo

We can still improve that https://nosercloud.sharepoint.com/SitePages/Vorschlagswesen.aspx

#### Also: Cheers to the power of Markdown
Presentation made with Marp https://github.com/yhatt/marp